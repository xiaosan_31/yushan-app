import App from './App'
import io from "@hyoga/uni-socket.io"
import {
	RuntimeServerUrl
} from "./commons/getRuntimeEnv.js"
let socketURL = RuntimeServerUrl

const socket = io(socketURL, {
	query: {},
	transports: ['websocket', 'polling'],
	timeout: 5000,
})
Vue.prototype.$socket = socket


// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3 
import {
	createSSRApp
} from 'vue'
import {
	log
} from './js_sdk/socket.io-client/dist/socket.io'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif

// 无法做路由 只能提前到Vue首次构造中的 生命周期中

// 优化策略  减少页面请求数量
// 1 用户的首页数据可以存到localStorage中 在用户退出程序时保存最新版，
// 2 用户再次进入时直接使用localStorage中的数据 不用在去请求服务器

// 体积优化 
// 封装组件 如nav导航栏的封装

// 工程化
// 1 将请求分离出来，做成一个公用的模块，减少重复代码

// 用户体验
// 合理的进行