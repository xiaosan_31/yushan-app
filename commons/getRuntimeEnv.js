import config from "./config.js"
//开发模式得到development地址 生产模式得到production
function getRuntimeServerUrl() {
	return process.env.NODE_ENV == 'development' ? config.development + '' : config.production + ''
}

export const RuntimeServerUrl = getRuntimeServerUrl()