import {
	RuntimeServerUrl
} from "../getRuntimeEnv.js"

// console.log(process.env.NODE_ENV,'process.env.NODE_ENV');

let baseURL = RuntimeServerUrl

export default {
	//时间显示格式化 接收一个时间对象
	// @params item 接收一个时间戳
	// @params type没有传值那么只显示时间
	// @params type传入值显示 上午 下午 昨天 年月日
	// 其实type是为了适应chat聊天显示数据
	formatDate: function(item, type) {
		let D = new Date()
		let nowDate = D.getTime()
		let customDate = item.getTime()
		if (nowDate - customDate > 86400000 * 360) {
			if (type) {
				return `${item.getFullYear()}年${item.getMonth()+1}月${item.getDate()}日 ${format(item.getHours())}:${format(item.getMinutes())}`
			}
		}
		// 时间如果聊天时间记录如果相差两天 那么显示年月日时分
		if (nowDate - customDate > 2 * 86400000) {
			if (type) {
				return `${item.getMonth()+1}月${item.getDate()}日 ${format(item.getHours())}:${format(item.getMinutes())}`
			} else {
				return `${item.getFullYear()}-${item.getMonth()+1}-${item.getDate()} ${format(item.getHours())}:${format(item.getMinutes())}`
			}
		}
		// 时间显示昨天 就是比较当前天数-之前天数如果大于1 显示昨天
		if (nowDate - customDate > 86400000) {
			return `昨天${format(item.getHours())}:${format(item.getMinutes())}`
		}
		// 显示小于一天 前面显示下午上午
		if (nowDate - customDate < 86400000) {
			if (type) {
				if (item.getHours() >= 12)
					return `${format(item.getHours())}:${format(item.getMinutes())}`
				else if (item.getHours() < 12)
					return `${format(item.getHours())}:${format(item.getMinutes())}`
				else
					return `${item.getHours()}:${item.getMinutes()}`
			} else {
				if (item.getHours() >= 12)
					return `下午${format(item.getHours())}:${format(item.getMinutes())}`
				else if (item.getHours() < 12)
					return `上午${format(item.getHours())}:${format(item.getMinutes())}`
				else
					return `${item.getHours()}:${item.getMinutes()}`
			}
		}

		function format(item) {
			return item < 10 ? '0' + item : item
		}
	},
	// 间隔时间显示
	// 第一次发信息的时间 第二次发信息的时间
	// 如果时间是在5分钟之内，那么返回一个'' 让其消失
	spaceTime(old, now) {
		// console.log(typeof old,typeof now);
		old = new Date(old).getTime()
		now = new Date(now).getTime()
		if (old > (now + 1000 * 60 * 5)) {
			return now
		} else {
			// 返回空 则是在5分钟内的消息，不显示
			return ''
		}
	},

	// 防抖
	debounce(fn, t = 500) {
		console.log(fn, 'fn');
		let timer = null;
		console.log(this);
		if (timer != null)
			clearTimeout(timer);
		timer = setTimeout(() => {
			// fn.call(this,arguments)
			fn()
			timer = null
		}, delay)
	},
	//验证邮箱
	checkEmailFormat(email) {
		let reg = new RegExp(/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/)
		// console.log(reg.test(email));
		return reg.test(email)
	},
	//验证手机号码
	checkPhoneNumberFormat(phone) {
		let reg = /^1(3\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\d|9[0-35-9])\d{8}$/
		return reg.test(phone)
	},
	//对uni.request进行promise化
	request(option) {
		let token = uni.getStorageSync('token');
		const {
			url,
			method,
			data,
			header,
			complete
		} = option
		return new Promise((resolve, reject) => {
			uni.request({
				url: `${baseURL}${url}`,
				method,
				header: {
					"Authorization": 'Bearer ' + token,
				},
				data,
				success(res) {
					//响应代码200为正常
					if (res.statusCode == 200)
						//res是响应体，数据一般在data中 res.data
						resolve(res)
					else
						reject(res)
				},
				fail(err) {
					reject(err)
				}
			})
		})
	}
}