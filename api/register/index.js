import util from "../../commons/js/util.js"

export const emailCheckApi = (email) => {
	return util.request({
		url: '/signup/judge',
		method: 'POST',
		data: {
			data: email,
			type: 'email'
		}
	})
}

export const validaCodeApi = ({
	code,
	email
}) => {
	return util.request({
		url: '/email/validaCode',
		method: 'POST',
		data: {
			code,
			email
		}
	})
}
export const registerApi = ({
	name,
	mail,
	pwd
}) => {
	return util.request({
		url: '/signup/add',
		method: 'POST',
		data: {
			name,
			mail,
			pwd
		}
	})
}

export const getEmailValidateCode = ({
	email
}) => {
	return util.request({
		url: "/email",
		method: "post",
		data: {
			email
		}
	})
}

export const updatePwd = ({
	email,
	newPwd
}) => {
	return util.request({
		url: "/user/forget",
		method: "post",
		data: {
			email,
			newPwd
		}
	})
}

//用户是否存在好友关系
export const hasRelationship = ({
	uid,
	fid
}) => {
	return util.request({
		url: "/search/isfriend",
		method: "post",
		data: {
			uid,
			fid
		}
	})
}

//用户是否存在
export const tobeOrNot = ({
	id
}) => {
	return util.request({
		url: "/user/detail",
		method: "post",
		data: {
			id
		}
	})
}