import util from "../../commons/js/util.js"

//获取用户详情信息 显示
export const getUserDetailApi = (id) => {
	return util.request({
		url: '/user/detail',
		method: "POST",
		data: {
			id,
		}
	})
}

// 删除好友
export const deleteFriendApi = (uid, fid) => {
	return util.request({
		url: '/friend/deletefriend',
		method: "POST",
		data: {
			uid,
			fid
		}
	})
}