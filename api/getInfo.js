import util from "../commons/js/util.js"
export function getUsetList() {
	return util.request({
		url: '/index/getUserList',
		method: "POST",
		data: {
			uid: this.userid,
			state: 0
		}
	})
}

export function checkLogin() {
	return util.request({
		url: '/signin/match',
		method: 'post',
		data: {
			data: this.Email,
			pwd: this.pwd
		}
	})
}

/**
 * 清除未读消息
 * @param {Object} uid 用户自己的id
 * @param {Object} fid 好友的id
 * @returns {Promise} 兑现时 结果为成功
 */
export function clearUnreadMsgApi(uid, fid) {
	return util.request({
		url: '/index/cleanunreadmsg_u',
		method: 'post',
		data: {
			uid,
			fid
		}
	})
}

/**
 * 获取未读消息
 * @param {Object} uid 用户自己的id
 * @param {Object} fid 好友的id
 * @returns {Promise}  兑现时 结果为一个整数
 */
export function getUnread(uid, fid) {
	return new Promise((resolve, reject) => {
		util.request({
			url: "/index/getunreadmsg",
			method: "POST",
			data: {
				uid,
				fid
			}
		}).then(res => {
			console.log(res.data.result);
			//返回请求得到的最后消息信息
			resolve(res.data.result)
		}).catch(err => {
			console.log(err);
		})
	})
}