import util from "../../commons/js/util.js"

export const robotMessage = (data) => {
	return util.request({
		url: '/robot',
		method: 'POST',
		data
	})
}
// export const genMessage = (data) => {
// 	return util.request({
// 		url: '/gen',
// 		method: 'post',
// 	})
// }
export const getRobotMsg = (id) => {
	return util.request({
		url: '/getrobmsg',
		method: 'POST',
		data: {
			id
		}
	})
}