import util from "../../commons/js/util.js"
/**
 *@param {string}  搜索用户关键字 
 */
export const getdata = (partKey) => {
	return new Promise((resolve, reject) => {
		util.request({
			url: '/search/user',
			method: "POST",
			data: {
				data: partKey
			}
		}).then(res => {
			// //console.log(res.data.result);
			if (res.data.status == 200) {
				resolve(res.data.result)
			} else {
				reject([])
			}
		}).catch(err => {
			//console.log(err);
		})
	})
}
/**
 * @param {Object} uid
 * @param {Object} fid
 */
export const isFriend = (uid, fid) => {
	return new Promise((resolve, reject) => {
		util.request({
			url: '/search/isfriend',
			method: "POST",
			data: {
				uid,
				fid,
			}
		}).then(res => {
			if (res.data.status == 200) {
				resolve(1)
			} else {
				resolve(0)
			}
		})
	})
}