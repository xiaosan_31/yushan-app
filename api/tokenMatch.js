import util from "../commons/js/util.js"



/**
 * 比对token凭证
 * @returns {Promise}  兑现时 返回 一个结果对象 对象中data.result=1 匹配成功 直接登入
 */
export default function tokenMatch() {
	return util.request({
		url: '/signin/testtoken',
		method: "POST",
		data: {
			token: uni.getStorageSync('token')
		}
	})
}